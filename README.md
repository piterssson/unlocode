## Virtual host

Set virtual host for example: 127.0.0.1  dev.unlocode

## Database

```sh
localhost:181
```

## Docker

Build project

```sh
docker-compose build
```

Run project

```sh
docker-compose up -d
```

Compose install

```sh
docker-compose exec php bash
composer install
```

## Run migrations

```sh
bin/console doctrine:migrations:migrate
```

## Import unlocodes

```sh
docker-compose exec php bash
bin/console app:import-unlocode
```

##  Update elasticsearch

```sh
bin/console fos:elastica:populate
```

#  Endpoints

### get location by code
```sh
/api/unlocode/locationbycode/{code}
```

### search location by NameWoDiacritics
```sh
/api/unlocode/locationbynamewodiacritics/{NameWoDiacritics} 
```