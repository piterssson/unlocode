<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Unlocode;

class ParseFile
{
    private $container;
    private $csv;
    private $em;

    public function __construct(ContainerInterface $container, Csv $csv, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->csv       = $csv;
        $this->em        = $em;
    }

    public function parse($dir)
    {
        $finder = new Finder();
        $dirzip = $this->container->getParameter('kernel.project_dir') . '/public/zip/'.$dir;
        $finder->files()->in($dirzip)->name('*CodeListPart*');

        foreach ($finder as $file) {
            $spreadsheet       = $this->csv->load($dirzip.'/'.$file->getFilename());
            $spreadsheetArray  = $spreadsheet->getActiveSheet()->toArray();
            foreach ($spreadsheetArray as $item) {

                $unlocode = $this->em->getRepository(Unlocode::class)->findOneBy(['code' => $item[1].''.$item[2]]);

                if(!$unlocode) {
                    $unlocode = new Unlocode();
                }
                $unlocode->setCh($item[0]);
                $unlocode->setCode($item[1].''.$item[2]);
                $unlocode->setName($item[3]);
                $unlocode->setNameWoDiacritics($item[4]);
                $unlocode->setSubDiv($item[5]);
                $unlocode->setFunction($item[6]);
                $unlocode->setStatus($item[7]);
                $unlocode->setDate($item[8]);
                $unlocode->setIata($item[9]);
                $unlocode->setCoordinates($item[10]);

                $this->em->persist($unlocode);
                $this->em->flush();
            }
        }
    }
}
