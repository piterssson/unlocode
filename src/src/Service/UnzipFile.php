<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use ZipArchive;

class UnzipFile
{
    private $container;
    private $zipArchive;
    private $filesystem;

    public function __construct(ContainerInterface $container, ZipArchive $zipArchive, Filesystem $filesystem)
    {
        $this->container  = $container;
        $this->zipArchive = $zipArchive;
        $this->filesystem = $filesystem;
    }

    public function unzip()
    {
        $zipFileUrl = $this->container->getParameter('zip_file_url');
        $date = new \DateTime('now');
        $filename = $date->format('Ymds');
        $dirzip = $this->container->getParameter('kernel.project_dir') . '/public/zip/';
        $file = $dirzip.''.$filename.'.zip';
        file_put_contents($file, file_get_contents($zipFileUrl));

        $fileZip = $this->zipArchive->open($file);
        if($fileZip === true) {
            $dir = $dirzip.''.$filename;
            $this->filesystem->mkdir($dir, 0777);
            $this->zipArchive->extractTo($dir);
            $this->zipArchive->close();

            return $filename;
        }
    }
}
