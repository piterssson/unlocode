<?php

namespace App\Service;

use App\Service\UnzipFile;
use App\Service\ParseFile;

class ImportService
{
    private $unzipFile;
    private $parseFile;

    public function __construct(UnzipFile $unzipFile, ParseFile $parseFile)
    {
        $this->unzipFile = $unzipFile;
        $this->parseFile = $parseFile;
    }

    public function import()
    {
        $dir = $this->unzipFile->unzip();
        $this->parseFile->parse($dir);
    }
}
