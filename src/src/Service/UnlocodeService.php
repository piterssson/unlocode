<?php

namespace App\Service;

use App\Repository\UnlocodeRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Elastica\Query\QueryString;
use Elastica\Query;

class UnlocodeService
{
    private $unlocodeRepository;
    private $container;

    public function __construct(UnlocodeRepository $unlocodeRepository, ContainerInterface $container)
    {
        $this->unlocodeRepository = $unlocodeRepository;
        $this->container          = $container;
    }

    public function getLocationByCode($code)
    {
        $query= $this->unlocodeRepository->getLocationByCode($code);

        return $query;
    }

    public function getLocationsByNameWoDiacritics($NameWoDiacritics)
    {
        $q = new QueryString($NameWoDiacritics);
        $q->setDefaultOperator('AND');
        $query = new Query($q);

        $elasticFinder = $this->container->get('fos_elastica.finder.unlocodes.unlocode');
        $resultsFinder = $elasticFinder->find($query, 10000);

        return $resultsFinder;
    }
}
