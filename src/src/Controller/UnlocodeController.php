<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Service\UnlocodeService;

class UnlocodeController extends AbstractFOSRestController
{
    private $unlocodeService;

    public function __construct(UnlocodeService $unlocodeService)
    {
        $this->unlocodeService = $unlocodeService;
    }

    /**
     * @Rest\View()
     * @Rest\Get("/api/unlocode/locationbycode/{code}")
     */
    public function getLocationByCode($code)
    {
        $result = $this->unlocodeService->getLocationByCode($code);
        $view   = $this->view($result);

        return $this->handleView($view, Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/api/unlocode/locationbynamewodiacritics/{NameWoDiacritics}")
     */
    public function getLocationsByNameWoDiacritics($NameWoDiacritics)
    {
        $result = $this->unlocodeService->getLocationsByNameWoDiacritics($NameWoDiacritics);
        $view   = $this->view($result);

        return $this->handleView($view, Response::HTTP_OK);
    }
}
