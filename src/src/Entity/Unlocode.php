<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UnlocodeRepository")
 * @JMS\ExclusionPolicy("all")
 */
class Unlocode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $ch;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"elastica"})
     * @JMS\Expose
     */
    private $nameWoDiacritics;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subDiv;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $function;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $iata;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Groups({"elastica"})
     * @JMS\Expose
     */
    private $coordinates;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCh(): ?string
    {
        return $this->ch;
    }

    public function setCh(?string $ch): self
    {
        $this->ch = $ch;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameWoDiacritics(): ?string
    {
        return $this->nameWoDiacritics;
    }

    public function setNameWoDiacritics(?string $nameWoDiacritics): self
    {
        $this->nameWoDiacritics = $nameWoDiacritics;

        return $this;
    }

    public function getSubDiv(): ?string
    {
        return $this->subDiv;
    }

    public function setSubDiv(?string $subDiv): self
    {
        $this->subDiv = $subDiv;

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(?string $function): self
    {
        $this->function = $function;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDate(): ?int
    {
        return $this->date;
    }

    public function setDate(?int $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getIata(): ?string
    {
        return $this->iata;
    }

    public function setIata(?string $iata): self
    {
        $this->iata = $iata;

        return $this;
    }

    public function getCoordinates(): ?string
    {
        return $this->coordinates;
    }

    public function setCoordinates(?string $coordinates): self
    {
        $this->coordinates = $coordinates;

        return $this;
    }
}
