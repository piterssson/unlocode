<?php

namespace App\Repository;

use App\Entity\Unlocode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Unlocode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Unlocode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Unlocode[]    findAll()
 * @method Unlocode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnlocodeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Unlocode::class);
    }

    public function getLocationByCode($code)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p.coordinates, p.nameWoDiacritics')
            ->where('p.code = :unlocode')
            ->setParameter('unlocode', $code)
            ->getQuery();

        return $qb->execute();
    }
}
