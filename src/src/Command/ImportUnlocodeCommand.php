<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Service\ImportService;

class ImportUnlocodeCommand extends Command
{
    protected static $defaultName = 'app:import-unlocode';

    private $importService;

    public function __construct(ImportService $importService)
    {
        parent::__construct();

        $this->importService = $importService;
    }

    protected function configure()
    {

        $this
            ->setDescription('Import Unlocode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->importService->import();
        $output->writeln('Import OK');
    }
}
